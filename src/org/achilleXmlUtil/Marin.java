package org.achilleXmlUtil;

public class Marin {
	
	String nom,
	       prenom;
	int  age;
	
	
	/*constructeurs*/
	public Marin() {
		
	}
	public Marin(String nom, String prenom) {
	   this.nom = nom;
	   this.prenom = prenom;
	}

	/*getteurs et setteurs */
	
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	/* method toString */
	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(age + "/n").append(nom + "/n").append(prenom + "/n");
		return sb.toString();
	}
	
}
