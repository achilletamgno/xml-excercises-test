package org.achilleXmlUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;


public class XMLutil {

	Document read(File xmlFile) throws DocumentException {
		
		SAXReader saxReader = new SAXReader();
	    //saxReader.read(xmlFile);                       /*Ctrl+1  raccourci magique  */
	    Document document = saxReader.read(xmlFile);
		return document;
	}
	
	Marin deserialize(Document doc){
	  
		Element rootElement = doc.getRootElement();
		int id = Integer.parseInt(rootElement.attributeValue("id"));
		String nom = rootElement.elementText("nom");
		String prenom = rootElement.elementText("pernom");
	    Integer age = Integer.parseInt(rootElement.attributeValue("age"));
	    
	    Marin marin = new Marin();
	    
	    marin.setNom(nom);
	    marin.setPrenom(prenom);
	    
		return marin;
	}
	
	public static void main(String[] args) throws DocumentException, IOException {
		
		/*un socket est un fichier d'instance File*/
	    File xmlFile = new File("files/marin.xml"); 
	    
	    Document document = new  XMLutil().read(xmlFile);
	    Marin marin = new XMLutil().deserialize(document);
	    
	    System.out.println(marin);
	    
	    
	  
	    Element rootElement = DocumentHelper.createElement("marin");
	    //Element rootElement = DocumentFactory.getInstance().createElement("marin");
	    rootElement.addAttribute("id","12");
	    rootElement.addElement("nom").addText("Surcouf");
	    rootElement.addElement("prenom").addText("Robert");
	    rootElement.addElement("age").addText("33");
	    
	    Element createdDocument = DocumentHelper.createElement("marin");
	    createdDocument.add(rootElement);
	    
	    //pretty Print
	    FileWriter writer = new FileWriter(new File("files/created-marin.xml"));
	    //createdDocument.create
	 }
	
}
