package org.achille;


import java.io.File;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class FirstXML {

	public static void main(String[] args) throws DocumentException {
		
		  System.out.println("Hello World !");
		
		/*un socket est un fichier d'instance File*/
       File xmlFile = new File("files/nom.xml"); 
       
       /* la ptite magie pour */
       SAXReader saxReader = new SAXReader();
       //saxReader.read(xmlFile);                       /*Ctrl+1  raccourci magique  */
       Document document = saxReader.read(xmlFile);
       Element rootElement = document.getRootElement();
       String name = rootElement.getName();
       
           System.out.println( "Le doc nom est : " + name);
       
       List<Attribute> attributes = rootElement.attributes();
       for (Attribute attribute : attributes){
    	   System.out.println(" ------------------------");
    	   System.out.println("nom attribut : "    + attribute.getName());
    	   System.out.println("Valeur attribute :" + attribute.getValue());
       }
       
       
       List<Element> elements = rootElement.elements();    /* liste d'�l�ment
                                                            * toujours parcourir 
                                                            * foreach
                                                            */
       for (Element subElement : elements){
    	   System.out.println(" ------------------------");
    	   System.out.println("sous-�l�ment : " + subElement.getName());
    	   System.out.println("contenu :" + subElement.getStringValue());
       }
	}

}
